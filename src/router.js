import { createRouter, createWebHistory } from 'vue-router'
import CompanyList from './pages/dashboard/CompanyList.vue'
import CompanyDetail from './components/company/CompanyBoard.vue'
import Login from './pages/login/Login.vue'
import { useStore } from './store'


const routes = [
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/',
    name: 'Home',
    component: CompanyList
  },
  {
    path: '/company/:id',
    name: 'CompanyBoard',
    component: CompanyDetail
  },
  // other routes...
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

router.beforeEach((to, from, next) => {
  const store = useStore()
  const isAuthenticated = store.isAuthenticated

  if(!isAuthenticated && to.name !== 'Login'){
    next({name: 'Login'})
  }else{
    next()
  }
})

export default router