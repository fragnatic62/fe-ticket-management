import { publicApiClient } from '../requests/publicAxiosClient';
import { apiClient } from '../requests/axiosClient';


class TMApiService {

    static apiClientService = apiClient
    static PublicApiClientService = publicApiClient;

    static async fetchCompanies({page=1, pageSize=100}) {
        /**
         * This method is used to fetch companies.
         * It takes in two parameters, page and pageSize.
         */
        const requestBody = {
        'page': page,
        'page_size': pageSize
        };
        const response = await this.apiClientService.post('fetch_companies', requestBody);
        return response.data.results;
    }

    static async signIn({ email, password }) {
        /**
         * This method is used to sign in the user.
         * It takes in two parameters, email and password.
         */
        const response = await this.PublicApiClientService.post('post_signin', { email, password });
        localStorage.setItem('token', response.data.auth_token);
        return true;
    }

    static async fetchBoardsByCompanyId({ companyId }){
        /**
         * This method is used to fetch boards by company id
         * It take single parameter companyId
         */
        const requestBody = {
            'company_id': companyId
        }
        const response = await this.apiClientService.post('fetch_board_by_company_id', requestBody)
        return response.data
    }

    static async updateBoard({id, position, name, description, companyId}){
        /**
         * This method is used to update board
         * Parameters: id, position, name, description, date_created, date_updated, company_id
         */
        const requestBody = {
            'id': id,
            'position': position,
            'name': name,
            'description': description,
            'company_id': companyId
        }
        const response = await this.apiClientService.post('post_update_board', requestBody)
        return response.data
    
    }

    static async updateTask({id, position, name, description, boardId}){
        /**
         * This method is used to update task
         * Parameters: id, position, name, description, date_created, date_updated, board_id
         */
        const requestBody = {
            'id': id,
            'position': position,
            'name': name,
            'description': description,
            'board_id': boardId
        }
        const response = await this.apiClientService.post('post_update_task', requestBody)
        return response.data
    }

    static async addNewTask({position, name, description, boardId}){
        /**
         * This method is used to add new task
         * Parameters: position, name, description, board_id
         */
        const requestBody = {
            'position': position,
            'name': name,
            'description': description,
            'board_id': boardId
        }
        const response = await this.apiClientService.post('post_create_task', requestBody)
        return response.data
    }

    static async addNewBoard({position, name, description, companyId}){
        /**
         * This method is used to add new board
         * Parameters: position, name, description, company_id
         */
        const requestBody = {
            'position': position,
            'name': name,
            'description': description,
            'company_id': companyId
        }
        const response = await this.apiClientService.post('post_create_board', requestBody)
        return response.data
    }

    static async batchUpdateTasks({tasks}){
        /**
         * This method is used to batch update tasks
         * Parameters: tasks
         */
        const requestBody = {
            'tasks': tasks
        }
        const response = await this.apiClientService.post('post_batch_update_tasks', requestBody)
        return response.data
    }

    static async batchUpdateBoards({boards}){
        /**
         * This method is used to batch update boards
         * Parameters: boards
         */
        const requestBody = {
            'boards': boards
        }
        const response = await this.apiClientService.post('post_batch_update_boards', requestBody)
        return response.data
    }

    static async addNewClientCompany({name, email, description}){
        /**
         * This method is used to add new client company
         * Parameters: name, description
         */
        const requestBody = {
            'name': name,
            'email': email,
            'description': description
        }
        const response = await this.apiClientService.post('post_create_company', requestBody)
        return response.data
    }

    static async logOut({token}){
        /**
         * This method is used to log out the user
         * Parameters: token
         */
        const requestBody = {
            'token': token
        }
        const response = await this.apiClientService.post('post_sign_out', requestBody)
        return response.data
    }
}

export const tmApiService = TMApiService;
