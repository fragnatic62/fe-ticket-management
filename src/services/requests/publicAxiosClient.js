import axios from 'axios';


class PublicApiClient {
    constructor() {

            if (!PublicApiClient.instance) {

            const apiClient = axios.create({
                baseURL: 'http://127.0.0.1:8000/public'
            });
            PublicApiClient.instance = apiClient;
            return PublicApiClient.instance;

        }
        return PublicApiClient.instance;

    }
}

export const publicApiClient = new PublicApiClient();