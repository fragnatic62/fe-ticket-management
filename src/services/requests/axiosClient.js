import axios from 'axios';

class AxiosClient {

  constructor() {
    /**
    * The singleton pattern is used to ensure that only one instance of the AxiosClient class is created.
    * This is important because we want to use the same instance of the AxiosClient class throughout the application.
    */

    if (!AxiosClient.instance) {
      const apiClient = axios.create({
        baseURL: 'http://127.0.0.1:8000/v1/',
      });

      apiClient.interceptors.request.use(config => {
        const token = localStorage.getItem('token');
        if (token) {
          config.headers['x-api-key'] = token;
        }
        return config;
      });

      apiClient.interceptors.response.use(response => response, this.handleResponseError);

     AxiosClient.instance = apiClient;

     return AxiosClient.instance;
    }
    return AxiosClient.instance;
  }

  handleResponseError(error) {
    /**
     * This method is used to handle the response error.
     * If the response status is 403, it means that the token has expired.
     */

    const originalRequest = error.config;
    if (error.response.status === 403 && !originalRequest._retry) {
      originalRequest._retry = true;
      const token = localStorage.getItem('token');
      return axios.post('http://127.0.0.1:8000/public/post_refresh_token', { token })
        .then(response => {
          if (response.status === 200) {
            localStorage.setItem('token', response.data.auth_token);
            return AxiosClient.instance(originalRequest);
          }
        });
    }
    return Promise.reject(error);
  }
}

export const apiClient = new AxiosClient();
