import { ref } from 'vue'
import { defineStore } from 'pinia'
import { tmApiService } from '../services/ticketManagement/apiClient'



export const useStore = defineStore({
    id: 'main',
    state: () => ({
        companies: ref([]),
        isAuthenticated: ref(false)
    }),
    actions:{
        async fetchCompanies({page=1, pageSize=100}){
            const companies = await tmApiService.fetchCompanies({page, pageSize})
            this.companies = companies
        },
        async signIn({email, password}){
            const success = await tmApiService.signIn({email, password})
            if(success){
                this.isAuthenticated = true
            }
        },
        async fetchBoardByCompanyId({companyId}){
            const boards = await tmApiService.fetchBoardsByCompanyId({companyId})
            return boards
        },

        async updateBoard({id, position, name, description, date_created, date_updated, companyId}){
            const response = await tmApiService.updateBoard({id, position, name, description, date_created, date_updated, companyId})
            return response
        },

        async updateTask({id, position, name, description, boardId}){

            const response = await tmApiService.updateTask({id, position, name, description, boardId})
            return response
        },

        async addNewTask({position, name, description, boardId}){
            const response = await tmApiService.addNewTask({position, name, description, boardId})
            return response
        },

        async addNewBoard({position, name, description, companyId}){
            const response = await tmApiService.addNewBoard({position, name, description, companyId})
            return response
        },

        async batchUpdateTasks({tasks}){
            const response = await tmApiService.batchUpdateTasks({tasks})
            return response
        },

        async batchUpdateBoards({boards}){
            const response = await tmApiService.batchUpdateBoards({boards})
            return response
        },

        async addNewClientCompany({name, email, description}){
            const response = await tmApiService.addNewClientCompany({name, email, description})
            this.companies.push(response)
            return response
        },

        async logOut({token}) {
            const response = await tmApiService.logOut({token})
            if(response){
                this.isAuthenticated = false
            }
        }
        
    },
})
