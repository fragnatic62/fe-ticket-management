import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import { createPinia } from 'pinia'
import router from './router'

// Import the Font Awesome library and icons
import { library } from '@fortawesome/fontawesome-svg-core'
import {
    faHome,
    faChartLine,
    faTable,
    faArrowRight,
    faArrowLeft,
    faBars
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

// Add the icons to the library
library.add(
    faHome, 
    faChartLine, 
    faTable, 
    faArrowRight, 
    faArrowLeft,
    faBars
)

createApp(App)
    .use(router)
    .use(createPinia())
    .component('font-awesome-icon', FontAwesomeIcon)
    .mount('#app')
